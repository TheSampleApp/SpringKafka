package com.the.sample.app.producer;

public interface EventProducer {
    void produce(String eventKey, String eventValue);
}
