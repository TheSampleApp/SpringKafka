package com.the.sample.app.producer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class EventProducerKafkaImpl implements EventProducer{
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final String topicName;

    public EventProducerKafkaImpl(KafkaTemplate<String, String> kafkaTemplate,
                                  @Value("${kafka.topic.name}") String topicName) {
        this.kafkaTemplate = kafkaTemplate;
        this.topicName = topicName;
    }

    @Override
    public void produce(String eventKey, String eventValue) {
        kafkaTemplate.send(topicName,eventKey,eventValue);
    }
}
