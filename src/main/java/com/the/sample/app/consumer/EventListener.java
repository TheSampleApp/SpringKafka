package com.the.sample.app.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EventListener {
    @KafkaListener(topics = "${kafka.topic.name}", groupId = "${kafka.consumer.group}")
    public void onEvent(String eventMessage){
        //process the event
        log.info("Message received:"+eventMessage);
    }
}
